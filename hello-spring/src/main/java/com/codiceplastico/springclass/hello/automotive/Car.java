package com.codiceplastico.springclass.hello.automotive;

public class Car {
	private Engine engine;
	private Brakes brakes;

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Brakes getBrakes() {
		return brakes;
	}

	public void setBrakes(Brakes brakes) {
		this.brakes = brakes;
	}
}
