package com.codiceplastico.springclass.hello;

public class HelloFromSource {
	private final MessageSource source;
	
	public HelloFromSource(MessageSource source) {
		this.source = source;
	}
	
	public String sayHello() {
		return String.format("%s, World!", source.getMessage());
	}

    public String sayHelloTo(String target) {
		return String.format("%s, %s!", source.getMessage(), target);
    }
}
