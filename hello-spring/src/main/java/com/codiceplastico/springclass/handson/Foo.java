package com.codiceplastico.springclass.handson;

public class Foo {
    private final Counter counter;

    public Foo(Counter counter) {
        this.counter = counter;
    }

    public void execute() {
       counter.increment();
    }
}
