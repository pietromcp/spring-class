package com.codiceplastico.springclass.hello;

import static org.junit.Assert.*;
import static org.hamcrest.core.StringContains.containsString;

import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutowireByConstructor {
	@Test
	public void autowireComponentsByConstructor() throws Exception {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("autowire-by-constructor.xml")) {
			GreetingsController ctrl = context.getBean(GreetingsController.class);
			String result = ctrl.greetings();
			assertThat(result, containsString("Hello, World!"));
			assertThat(result, containsString("Hi, World!"));
			assertThat(result, containsString("Hola, World!"));
			assertThat(result, containsString(";"));
		}		
	}
}
