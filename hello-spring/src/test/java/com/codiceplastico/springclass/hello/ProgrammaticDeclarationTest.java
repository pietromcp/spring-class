package com.codiceplastico.springclass.hello;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.support.StaticApplicationContext;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class ProgrammaticDeclarationTest {
    @Test
    public void declareContextBeansProgrammatically() {
        StaticApplicationContext context = buildContext();

        Hello hello = context.getBean(Hello.class);
        Assert.assertEquals("Hello, World!", hello.sayHello());
    }

    @Test
    public void registerBeanDefinitionsUsingBuilder() {
        GenericApplicationContext ctx = new GenericApplicationContext();

        BeanDefinition handlerDef = BeanDefinitionBuilder.rootBeanDefinition(GreetingsHandler.class)
                .setAutowireMode(AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR).getBeanDefinition();

        BeanDefinition helloDef = BeanDefinitionBuilder.rootBeanDefinition(Hello.class)
                .setAutowireMode(AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR).getBeanDefinition();

        ctx.registerBeanDefinition("hello", helloDef);
        ctx.registerBeanDefinition("handler", handlerDef);
        ctx.refresh();

        GreetingsHandler handler = ctx.getBean(GreetingsHandler.class);
        Assert.assertThat(handler, is(notNullValue()));
    }

    private StaticApplicationContext buildContext() {
        StaticApplicationContext context = new StaticApplicationContext();
        context.registerSingleton("Hello", Hello.class);
        return context;
    }
}
