package com.codiceplastico.springclass.hello;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.codiceplastico.springclass.hello.annotations.ColorController;

public class AutowireUsingAnnotation {
	@Test
	public void autowireComponentsUsingAnnotation() throws Exception {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("autowire-using-annotation-context.xml")) {
			ColorController ctrl = context.getBean(ColorController.class);
			assertThat(ctrl, is(notNullValue()));
			assertThat(ctrl.red(), is(notNullValue()));
			assertThat(ctrl.blue(), is(notNullValue()));
			assertThat(ctrl.green(), is(notNullValue()));
		}
	}
	
	@Test
	public void autowireComponentsUsingAnnotationWithoutXmlConfiguration() throws Exception {
		try(AbstractApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class)) {
			ColorController ctrl = context.getBean(ColorController.class);
			assertThat(ctrl, is(notNullValue()));
			assertThat(ctrl.red(), is(notNullValue()));
			assertThat(ctrl.blue(), is(notNullValue()));
			assertThat(ctrl.green(), is(notNullValue()));
		}
	}
	
	@Configuration
	@ComponentScan(basePackages="com.codiceplastico.springclass.hello.annotations")
	public static class SpringConfiguration {		
	}
}
