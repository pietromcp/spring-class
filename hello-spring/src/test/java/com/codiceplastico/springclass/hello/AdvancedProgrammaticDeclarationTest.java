package com.codiceplastico.springclass.hello;

import com.codiceplastico.springclass.isolated.ContextConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

public class AdvancedProgrammaticDeclarationTest {
    @Test
    public void useAnnotationConfigApplicationContext() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        GreetingsHandler handler = ctx.getBean(GreetingsHandler.class);
        Assert.assertThat(handler, is(notNullValue()));
    }
}

