package com.codiceplastico.springclass.hello;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.codiceplastico.springclass.hello.automotive.Car;

public class AutowireByNameTest {
	@Test
	public void autowireComponentsByConstructor() {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("autowire-by-name-context.xml")) {
			Car car = context.getBean(Car.class);
			assertThat(car, is(notNullValue()));
			assertThat(car.getEngine(), is(notNullValue()));
			assertThat(car.getBrakes(), is(notNullValue()));
		}
	}
}
