package com.codiceplastico.springclass.hello;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.*;

import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AdvancedGetTest {
	@Test
	public void getComponentByName() throws Exception {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("advanced-get-context.xml")) {
			Hello hello = (Hello)context.getBean("hola");
			Assert.assertThat(hello.sayHello(), is("Hola, World!"));
		}
	}
	
	@Test
	public void getComponentListByName() throws Exception {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("advanced-get-context.xml")) {
			Map<String, Hello> beans = context.getBeansOfType(Hello.class);
			Assert.assertThat(beans.size(), is(3));
			Assert.assertThat(beans.keySet(), hasItems("hello", "hola", "hi"));
		}
	}
	
	@Test
	public void injectComponentList() throws Exception {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("advanced-get-context.xml")) {
			GreetingsHandler handler = context.getBean(GreetingsHandler.class);
			Assert.assertThat(handler.greetingsCount(), is(3));
		}
	}

	@Test(expected = NoUniqueBeanDefinitionException.class)
	public void getComponentByTypeObtainingException() throws Exception {
		try(AbstractApplicationContext context = new ClassPathXmlApplicationContext("advanced-get-context.xml")) {
			Hello handler = context.getBean(Hello.class);
		}
	}
}

