package com.codiceplastico.springclass.hello;

import org.junit.*;

public class HelloTest {
	@Test
	public void shouldSayHelloWorld() {
		Hello sut = new Hello("Hello");
		Assert.assertEquals("Hello, World!", sut.sayHello());
	}
}