package com.codiceplastico.springclass.isolated;

import com.codiceplastico.springclass.hello.GreetingsHandler;
import com.codiceplastico.springclass.hello.Hello;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;

@Configuration
public class ContextConfiguration {
    @Bean
    public Hello helloWorld(){
        return new Hello();
    }

    @Bean
    public GreetingsHandler handler(Collection<Hello> hellos) {
        return new GreetingsHandler(hellos);
    }
}
