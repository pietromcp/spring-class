package com.codiceplastico.springclass.handson;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HandsOn02CollaboratorsTest {
    @Test
    public void sameCounterInstanceInjectedIntoFooAndBarBeans() {
        try (AbstractApplicationContext context = new ClassPathXmlApplicationContext("handson/02-collaborators-context.xml")) {
//            int fooInvocationCount = 100;
//            int barInvocationCount = 70;
//            Foo foo = context.getBean(Foo.class);
//            for (int i = 0; i < fooInvocationCount; i++) {
//                foo.execute();
//            }
//            Bar bar = context.getBean(Bar.class);
//            for (int i = 0; i < barInvocationCount; i++) {
//                bar.execute();
//            }
//            Counter counter = context.getBean(Counter.class);
//            assertThat(counter.getValue(), is(fooInvocationCount + barInvocationCount));
        }
    }
}
