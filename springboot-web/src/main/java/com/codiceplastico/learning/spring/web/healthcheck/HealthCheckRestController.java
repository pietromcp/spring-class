package com.codiceplastico.learning.spring.web.healthcheck;

import com.codiceplastico.learning.spring.web.clock.Clock;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/rest/health-check")
public class HealthCheckRestController {
    private final Clock clock;

    public HealthCheckRestController(Clock clock) {
        this.clock = clock;
    }

    @RequestMapping("")
    public HealthCheckResult check() {
        return HealthCheckResult.ok(clock.now());
    }

    public static final class HealthCheckResult {
        private boolean status;
        private ZonedDateTime occurredOn;

        private HealthCheckResult(boolean status, ZonedDateTime occurredOn) {
            this.status = status;
            this.occurredOn = occurredOn;
        }

        public HealthCheckResult() {
        }

        public boolean isOk() {
            return status;
        }

        public boolean getStatus() {
            return status;
        }

        public ZonedDateTime getOccurredOn() {
            return occurredOn;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public void setOccurredOn(ZonedDateTime occurredOn) {
            this.occurredOn = occurredOn;
        }

        public static HealthCheckResult ok(ZonedDateTime occurredOn) {
            return new HealthCheckResult(true, occurredOn);
        }
    }
}
