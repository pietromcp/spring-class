package com.codiceplastico.learning.spring.web.clock;

import java.time.ZonedDateTime;

public interface Clock {
    ZonedDateTime now();
}
