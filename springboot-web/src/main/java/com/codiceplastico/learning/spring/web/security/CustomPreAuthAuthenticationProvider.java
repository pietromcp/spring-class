package com.codiceplastico.learning.spring.web.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Collection;
import java.util.HashSet;

public class CustomPreAuthAuthenticationProvider implements AuthenticationProvider {
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        CustomUserDetails details = (CustomUserDetails) authentication.getDetails();
        Object credentials = authentication.getCredentials();
        if (isUserValid(details)) {
            return buildAuthenticationToken(details, credentials);
        }
        return null;
    }

    private Authentication buildAuthenticationToken(CustomUserDetails details, Object credentials) {
        final Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_PREAUTH_USER"));
        return new PreAuthenticatedAuthenticationToken(buildPricipal(details), credentials, authorities);
    }

    private Object buildPricipal(CustomUserDetails details) {
        return new CustomUserPrincipal(details);
    }

    private boolean isUserValid(CustomUserDetails details) {
        return details.getUsername().startsWith("pietro");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(PreAuthenticatedAuthenticationToken.class);
    }
}