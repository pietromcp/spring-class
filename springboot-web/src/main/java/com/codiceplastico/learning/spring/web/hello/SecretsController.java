package com.codiceplastico.learning.spring.web.hello;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("secrets")
public class SecretsController {
    @RequestMapping(value="", method = RequestMethod.GET)
    public ModelAndView greet(String target, Authentication authentication, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("secrets");
        mav.addObject("remoteUser", authentication.getName());
        mav.addObject("contextPath", request.getContextPath());
        return mav;
    }
}
