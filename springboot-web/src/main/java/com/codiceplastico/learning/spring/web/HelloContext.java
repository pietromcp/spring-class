package com.codiceplastico.learning.spring.web;

import com.codiceplastico.springclass.hello.FixedMessageSource;
import com.codiceplastico.springclass.hello.HelloFromSource;
import com.codiceplastico.springclass.hello.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloContext {
    @Bean
    public HelloFromSource helloFromSource(MessageSource source) {
        return new HelloFromSource(source);
    }

    @Bean
    public MessageSource myMessageSource() {
        return new FixedMessageSource("Hello");
    }
}
