package com.codiceplastico.learning.spring.web.clock;

import java.time.ZonedDateTime;

public class SystemClock implements Clock {
    @Override
    public ZonedDateTime now() {
        return ZonedDateTime.now();
    }
}
