package com.codiceplastico.learning.spring.web.security;

import java.util.Collection;

public class InMemoryUser {
    public final String username;
    public final String password;
    public final String companyId;
    public final Collection<String> roles;

    public InMemoryUser(String username, String password, String companyId, Collection<String> roles) {
        this.username = username;
        this.password = password;
        this.companyId = companyId;
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCompanyId() {
        return companyId;
    }

    public Collection<String> getRoles() {
        return roles;
    }
}
