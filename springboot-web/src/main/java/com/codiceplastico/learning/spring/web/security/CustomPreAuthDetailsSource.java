package com.codiceplastico.learning.spring.web.security;

import org.springframework.security.authentication.AuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;

public class CustomPreAuthDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, CustomUserDetails> {
    @Override
    public CustomUserDetails buildDetails(HttpServletRequest request) {
        String username = request.getHeader("__the_username");
        String company = request.getHeader("__the_company");
        CustomUserDetails details = new CustomUserDetails(username, company);
        return details;
    }
}
