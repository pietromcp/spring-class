package com.codiceplastico.learning.spring.web.security;

public class CustomUserPrincipal {
    private final CustomUserDetails details;

    public CustomUserPrincipal(CustomUserDetails details) {
        this.details = details;
    }

    public CustomUserDetails getDetails() {
        return details;
    }

    @Override
    public String toString() {
        return String.format("[CustomUserPrincipal - DETAILS: %s]", details.toString());
    }
}
