package com.codiceplastico.learning.spring.web.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;

public class CustomAuthenticationProvider implements AuthenticationProvider {
    private final Collection<InMemoryUser> users;

    public CustomAuthenticationProvider(Collection<InMemoryUser> users) {
        this.users = users;
    }
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken)authentication;
        String username = auth.getName();
        String password = (String) auth.getCredentials();
        for (InMemoryUser user : users) {
            if(user.getUsername().equals(username)
                    && user.getPassword().equals(password)) {
                return buildAuthentication(user);
            }
        }
        return null;
    }

    private Authentication buildAuthentication(InMemoryUser user) {
        return new UsernamePasswordAuthenticationToken(buildPrincipal(user), user.getPassword(), buildAuthorities(user));
    }
    private Object buildPrincipal(InMemoryUser user) {
        return new CustomUserPrincipal(new CustomUserDetails(user.getUsername(), user.getCompanyId()));
    }

    private Collection<? extends GrantedAuthority> buildAuthorities(InMemoryUser user) {
        Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for (String role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
