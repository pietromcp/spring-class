package com.codiceplastico.learning.spring.web.security;

public class CustomUserDetails {
    private final String username;
    private final String companyId;

    public CustomUserDetails(String username, String companyId) {
        this.username = username;
        this.companyId = companyId;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyId() {
        return companyId;
    }

    @Override
    public String toString() {
        return "CustomUserDetails{" +
                "username='" + username + '\'' +
                ", companyId='" + companyId + '\'' +
                '}';
    }
}
