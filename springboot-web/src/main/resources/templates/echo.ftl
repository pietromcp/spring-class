<#import "/spring.ftl" as spring/>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Echo message</title>
    </head>
    <body>
        <h2>${text}</h2>
        <h3><@spring.message "subtitle" /></h3>
        <h3>Color is: <@spring.theme "color" /></h3>
    </body>
</html>
