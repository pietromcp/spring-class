<#import "/spring.ftl" as spring/>
<!DOCTYPE HTML>
<html>
<head>
<title><@spring.message "hello.title" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<p>
		Hello <b>${remoteUser}</b>
	</p>
	<p>Only administrators can read these secrets!</p>
    	<p>
    		<a href="${contextPath}/greetings/greet?target=${remoteUser}">Greetings page</a>
    	</p>

	<form class="form-inline" action="<@spring.url "/custom-logout-url" />" method="post">
		<input type="submit" value="Log out" />
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</body>
</html>