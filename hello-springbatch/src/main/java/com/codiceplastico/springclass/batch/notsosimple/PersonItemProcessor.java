package com.codiceplastico.springclass.batch.notsosimple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class PersonItemProcessor implements ItemProcessor<CsvPerson, DbPerson> {
    private static final Logger log = LoggerFactory.getLogger(PersonItemProcessor.class);
    @Override
    public DbPerson process(final CsvPerson person) throws Exception {
        final String firstName = person.getFirstName().toUpperCase();
        final String lastName = person.getLastName().toUpperCase();

        final DbPerson transformedPerson = new DbPerson(firstName, lastName, firstName.substring(0,  1) + lastName.substring(0,  1));

        log.info("Converting (" + person + ") into (" + transformedPerson + ")");

        return transformedPerson;
    }
}
