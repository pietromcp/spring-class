package com.codiceplastico.springclass.batch.notsosimple;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;

    // tag::readerwriterprocessor[]
//    @Bean
    public FlatFileItemReader<CsvPerson> reader() {
        FlatFileItemReader<CsvPerson> reader = new FlatFileItemReader<CsvPerson>();
        reader.setResource(new ClassPathResource("data.csv"));
        reader.setLineMapper(new DefaultLineMapper<CsvPerson>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "firstName", "lastName" });
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<CsvPerson>() {{
                setTargetType(CsvPerson.class);
            }});
        }});
        return reader;
    }

//    @Bean
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<DbPerson> writer() {
        JdbcBatchItemWriter<DbPerson> writer = new JdbcBatchItemWriter<DbPerson>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<DbPerson>());
        writer.setSql("INSERT INTO people (first_name, last_name, initials) VALUES (:firstName, :lastName, :initials)");
        writer.setDataSource(dataSource);
        return writer;
    }
    // end::readerwriterprocessor[]

    // tag::jobstep[]
    @Bean
    public Job importUserJob(JobExecutionListener listener) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<CsvPerson, DbPerson> chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }
    // end::jobstep[]
    
    @Bean(destroyMethod = "close")
    DataSource dataSource(Environment env) {
        HikariConfig dataSourceConfig = new HikariConfig();
 
        dataSourceConfig.setDriverClassName("org.postgresql.Driver");
        dataSourceConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSourceConfig.setUsername("postgres");
        dataSourceConfig.setPassword("mysecretpassword");
 
        HikariDataSource dataSource = new HikariDataSource(dataSourceConfig);
        DatabasePopulatorUtils.execute(createDatabasePopulator(env), dataSource);
        DatabasePopulatorUtils.execute(createSpringBatchDatabasePopulator(env), dataSource);
        
        return dataSource;
    }
    
    private DatabasePopulator createDatabasePopulator(Environment env) {
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
        databasePopulator.setContinueOnError(true);
        databasePopulator.addScript(new ClassPathResource("schema-" + env.getProperty("app.db-dialect") + ".sql"));
        return databasePopulator;
    }
    
    private DatabasePopulator createSpringBatchDatabasePopulator(Environment env) {
    	ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
    	databasePopulator.setContinueOnError(true);
    	databasePopulator.addScript(new ClassPathResource("org/springframework/batch/core/schema-" + env.getProperty("app.db-dialect") + ".sql"));
    	return databasePopulator;
    }
     
    @Bean
    NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }
}
