package com.codiceplastico.springclass.batch.notsosimple;

public class DbPerson {
	private int id;
	private String lastName;
	private String firstName;
	private String initials;

	public DbPerson(int id, String lastName, String firstName, String initials) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.initials = initials;
	}
	
	public DbPerson(String lastName, String firstName, String initials) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.initials = initials;
	}
	
	public int getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getInitials() {
		return initials;
	}

	@Override
	public String toString() {
		return "DbPerson [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", initials=" + initials + "]";
	}
}
