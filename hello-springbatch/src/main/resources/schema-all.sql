DROP TABLE IF EXISTS people;

CREATE TABLE people  (
    id SERIAL NOT NULL PRIMARY KEY,
    first_name VARCHAR(20),
    last_name VARCHAR(20),
    initials VARCHAR(2)
);