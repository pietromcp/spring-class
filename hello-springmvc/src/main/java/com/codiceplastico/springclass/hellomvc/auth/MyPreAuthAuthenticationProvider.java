package com.codiceplastico.springclass.hellomvc.auth;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

public class MyPreAuthAuthenticationProvider implements AuthenticationProvider {
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		MyUserDetails details = (MyUserDetails) authentication.getDetails();
		Object credentials = authentication.getCredentials();
		if (isUserValid(details)) {
			return buildAuthenticationToken(details, credentials);
		}
		return null;
	}

	private Authentication buildAuthenticationToken(MyUserDetails details, Object credentials) {
		final Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_PREAUTH_USER"));
		return new PreAuthenticatedAuthenticationToken(buildPricipal(details), credentials, authorities);
	}

	private Object buildPricipal(MyUserDetails details) {
		return new MyUserPrincipal(details);
	}

	private boolean isUserValid(MyUserDetails details) {
		return "pietro".equals(details.getUsername());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(PreAuthenticatedAuthenticationToken.class);
	}
}
