package com.codiceplastico.springclass.hellomvc.auth;

public class MyUserPrincipal {
	private final MyUserDetails details;

	public MyUserPrincipal(MyUserDetails details) {
		this.details = details;
	}

	public String getCompanyId() {
		return details.getCompanyId();
	}
}