package com.codiceplastico.springclass.hellomvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.codiceplastico.springclass.hello.HelloFromSource;

@Controller
@RequestMapping("greetings")
public class ClassicHelloController {
	private final HelloFromSource helloService;
    @Autowired
    public ClassicHelloController(HelloFromSource helloService) {
        this.helloService = helloService;
    }
    
    @RequestMapping(value="/greet", method = RequestMethod.GET)
    public ModelAndView greet(String target, Authentication authentication ) {
    	final String message = helloService.sayHelloTo(target);
    	final ModelAndView mav = new ModelAndView("greetings");
    	mav.addObject("message", message);
    	mav.addObject("roles", authentication.getAuthorities());
    	mav.addObject("userInfo", authentication.getPrincipal());
		return mav;
    }
}
