package com.codiceplastico.springclass.hellomvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.codiceplastico.springclass.hello.HelloFromSource;

@Controller
@RequestMapping("secrets")
public class SecretsController {
    @RequestMapping(value="", method = RequestMethod.GET)
    public ModelAndView greet(String target, Authentication authentication ) {
		return new ModelAndView("secrets");
    }
}
