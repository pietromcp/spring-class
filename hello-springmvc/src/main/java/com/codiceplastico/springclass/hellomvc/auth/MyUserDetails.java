package com.codiceplastico.springclass.hellomvc.auth;

public class MyUserDetails {
	private final String username;
	private final String companyId;

	public MyUserDetails(String username, String companyId) {
		this.username = username;
		this.companyId = companyId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getCompanyId() {
		return companyId;
	}
}
