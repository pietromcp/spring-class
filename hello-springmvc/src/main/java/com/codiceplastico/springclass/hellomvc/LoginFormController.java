package com.codiceplastico.springclass.hellomvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("login-form")
public class LoginFormController {
    @RequestMapping(value="", method = RequestMethod.GET)
    public ModelAndView loginForm() {
    	return new ModelAndView("login-form");
    }
}
