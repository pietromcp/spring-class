package com.codiceplastico.springclass.boot;

import java.time.LocalDate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("person")
@EnableAutoConfiguration
public class StandaloneRestController {
    @RequestMapping(method = RequestMethod.GET)
    public Person getMessageFor() {
        return new Person("Pietro", "Martinelli", LocalDate.of(1978, 3, 19));
    }
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(StandaloneRestController.class, args);
    }
}
