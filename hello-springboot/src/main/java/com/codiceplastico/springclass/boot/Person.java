package com.codiceplastico.springclass.boot;

import java.time.LocalDate;

public class Person {
	private final String firstName;
	private final String lastName;
	private final LocalDate birthday;

	public Person(String firstName, String lastName, LocalDate birthday) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public String getFullName() {
		return String.format("%s %s", getFirstName(), getLastName());
	}
}
