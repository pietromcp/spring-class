package com.codiceplastico.learning.spring;

import com.codiceplastico.learning.spring.web.WebApplication;
import com.codiceplastico.learning.spring.web.healthcheck.HealthCheckController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class ApplicationTest {
    @Autowired
    private HealthCheckController controller;

    @Test
    public void contextLoads() throws Exception {
        Assert.assertThat(controller, is(notNullValue()));
    }
}
