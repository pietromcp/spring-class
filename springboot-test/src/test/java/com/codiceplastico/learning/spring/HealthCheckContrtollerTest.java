package com.codiceplastico.learning.spring;
import com.codiceplastico.learning.spring.web.WebApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
@AutoConfigureMockMvc
public class HealthCheckContrtollerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnOk() throws Exception {
        this.mockMvc.perform(get("/health-check/echo?text=Test text"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void shouldEchoProvidedMessage() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/health-check/echo?text=Test text"))
                .andReturn();
        assertThat(result.getResponse().getContentAsString(), containsString("Test text"));
    }

    @Test
    public void shouldEchoProvidedMessage_Alternative() throws Exception {
        this.mockMvc.perform(get("/health-check/echo?text=Test text"))
                .andDo(print())
                .andExpect(content().string(containsString("Test text")));
    }
}
