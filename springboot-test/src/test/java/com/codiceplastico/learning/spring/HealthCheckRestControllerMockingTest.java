package com.codiceplastico.learning.spring;

import com.codiceplastico.learning.spring.web.WebApplication;
import com.codiceplastico.learning.spring.web.clock.Clock;
import com.codiceplastico.learning.spring.web.healthcheck.HealthCheckRestController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
@AutoConfigureMockMvc
public class HealthCheckRestControllerMockingTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Clock clock;

    @Test
    public void shouldOccurredOnFromClock() throws Exception {
        when(clock.now()).thenReturn(ZonedDateTime.of(1978, 3, 19, 1, 15, 0, 0, ZoneId.of("UTC+1")));

        this.mockMvc.perform(get("/rest/health-check"))
                .andExpect(content().string(containsString("\"occurredOn\":\"1978-03-19")));
    }
}
